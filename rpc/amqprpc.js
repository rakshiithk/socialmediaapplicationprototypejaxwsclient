var crypto = require('crypto');
var soap = require('soap');

var TIMEOUT=4000; 
var CONTENT_TYPE='application/json';
var CONTENT_ENCODING='utf-8';
var self;

function AmqpRpc(){
	  self = this;
	  console.log("Connecting to JAXWS Server!!");
	}

var options = {
		ignoredNamespaces: true
		}
 
exports = module.exports = AmqpRpc;

AmqpRpc.prototype.makeRequest = function(operation , sql_stmt, parameters, callback){
	
  self = this;
 
  var correlationId = crypto.randomBytes(16).toString('hex');
  
  var tId = setTimeout(function(corr_id){
    //if this ever gets called we didn't get a response in a timely fashion
    callback(new Error("timeout " + corr_id));
    //delete the entry from hash
    delete self.requests[corr_id];
  }, TIMEOUT, correlationId);
 
  //create a request entry to store in a hash
  var entry = {
    callback:callback,
    timeout: tId //the id for the timeout so we can clear it
  };
  
  //put the entry in the hash so we can match the response later
  self.requests[correlationId]=entry;
	  
    //put the request on a queue
  switch(operation){
  case "loginUser" :
  case "verifyUser":
	  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
	  var args = {arg0: sql_stmt ,arg1: parameters};
	  soap.createClient(url,options, function(err, client) {
	  	//console.log(client.describe().HelloTomEEService.HelloTomEEPort.sum);
	        client.UserOperation.UserOperationsPort.loginUser(args,function(err, result) {
	        	console.log(err);
	        	result = JSON.parse(result);
	            console.log("Response for web service " + operation + " is ==>" + JSON.stringify(result));    
	            console.log(client.lastRequest);
	            //get the correlationId
	            var correlationId = m.correlationId;
	            //is it a response to a pending request
	            if(correlationId in self.requests){
	              //retreive the request entry
	              var entry = self.requests[correlationId];
	              //make sure we don't timeout by clearing it
	              clearTimeout(entry.timeout);
	              //delete the entry from hash
	              delete self.requests[correlationId];
	              //callback, no err
	              entry.callback(null, result);
	            }
	        });
	    });
	  break;
	  
  default :
	  break;
  }
};
