var crypto = require('crypto');
var soap = require('soap');

var TIMEOUT=10000; 
var CONTENT_TYPE='application/json';
var CONTENT_ENCODING='utf-8';
var self = this;
var requests = {}; 
var options = {
		//ignoredNamespaces: true
		}

exports.makeRequest = function(operation , sql_stmt, parameters, callback){
	try{
		  self = this;
		 
		  var correlationId = crypto.randomBytes(16).toString('hex');
		  
		  var tId = setTimeout(function(corr_id){
		    //if this ever gets called we didn't get a response in a timely fashion
		    callback(new Error("timeout " + corr_id));
		    //delete the entry from hash
		    delete requests[corr_id];
		  }, TIMEOUT, correlationId);
		 
		  //create a request entry to store in a hash
		  var entry = {
		    callback:callback,
		    timeout: tId //the id for the timeout so we can clear it
		  };
		  
		  //put the entry in the hash so we can match the response later
		  requests[correlationId]=entry;
			  
		    //put the request on a queue
		  switch(operation){
		  
		  case "loginUser" :
		  case "verifyUser":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.loginUser(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loginUser ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result.QueryResult[0]);
			            }
			        });
			    });
			  break;
			  
		  case "createUser":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.createUser(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("createUser ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result.QueryResult[0]);
			            }
			        });
			    });
			  break;
			  
		  case "getNewsFeed":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.getNewsFeed(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("getNewsFeed ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "postStatusUpdate":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.postStatusUpdate(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("postStatusUpdate ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "loadFriendList":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.loadFriendList(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loadFriendList ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "sendFiendRequest":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.sendFiendRequest(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("sendFiendRequest ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "renderFriendListPage":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.renderFriendListPage(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loginUser ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result.QueryResult[0]);
			            }
			        });
			    });
			  break;
			  
		  case "loadMyFriendList":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.loadMyFriendList(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loadMyFriendList ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "loadPendingFriendList":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.loadPendingFriendList(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loadPendingFriendList ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "rejectFriendRequest":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.rejectFriendRequest(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("rejectFriendRequest ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "acceptFriendRequest":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.acceptFriendRequest(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("acceptFriendRequest ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "renderUserDetailsPage":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.renderUserDetailsPage(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("renderUserDetailsPage ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result.QueryResult[0]);
			            }
			        });
			    });
			  break;
			  
		  case "getLifeEvents":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.getLifeEvents(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loadPendingFriendList ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "updateProfilePicture":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.updateProfilePicture(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("acceptFriendRequest ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "renderGroupsPage":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.renderGroupsPage(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("renderUserDetailsPage ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result.QueryResult[0]);
			            }
			        });
			    });
			  break;
			  
		  case "loadAllGroups":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.loadAllGroups(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loadPendingFriendList ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "loadMyGroups":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.loadMyGroups(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loadPendingFriendList ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "addUserToGroup":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.addUserToGroup(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("rejectFriendRequest ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "removeUserFromGroup":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.removeUserFromGroup(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("rejectFriendRequest ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "createGroup":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.createGroup(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("createGroup ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "unFriendUserRequest":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.unFriendUserRequest(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("createGroup ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "navToGroupDetailPage":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.navToGroupDetailPage(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loginUser ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result.QueryResult[0]);
			            }
			        });
			    });
			  break;
			  
		  case "getGroupDetails":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.getGroupDetails(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("getGroupDetails ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "getGroupUserList":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.getGroupUserList(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("getGroupUserList ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "getGroupNonMembers":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.getGroupNonMembers(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("getGroupUserList ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "addUserToGroupAdmin":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.addUserToGroupAdmin(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("addUserToGroupAdmin ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "removeUserFromGroupAdmin":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.removeUserFromGroupAdmin(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("addUserToGroupAdmin ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "deleteGroup":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.deleteGroup(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("deleteGroup ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  case "navToFriendDetailPage":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.navToFriendDetailPage(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("loginUser ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result.QueryResult[0]);
			            }
			        });
			    });
			  break;
			  
		  case "getFriendDetails":
			  var url = 'http://localhost:8080/SocialMediaPrototypeJAXWSServer/webservices/UserOperationsImplementation?wsdl';
			  var args = {arg0: sql_stmt ,arg1: JSON.stringify(parameters),arg2: correlationId};
			  soap.createClient(url,options, function(err, client) {
			  	console.log(client.describe());
			        client.UserOperation.UserOperationsPort.getFriendDetails(args,function(err, result) {
			        	console.log(err);
			        	result = JSON.parse(result.return);
			            console.log("getGroupUserList ==> Response for web service " + operation + " is ==>" + JSON.stringify(result));    
			            console.log(client.lastRequest);
			            //get the correlationId
			            var correlationId = result.correlationId;
			            //is it a response to a pending request
			            if(correlationId in requests){
			              //retreive the request entry
			              var entry = requests[correlationId];
			              //make sure we don't timeout by clearing it
			              clearTimeout(entry.timeout);
			              //delete the entry from hash
			              delete requests[correlationId];
			              //callback, no err
			              entry.callback(null, result);
			            }
			        });
			    });
			  break;
			  
		  default :
			  break;
		  }
	}catch(e){
		console.log(e)
	}
};
