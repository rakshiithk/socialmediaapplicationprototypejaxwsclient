var soap = require('../rpc/soap')
var async = require('async');
var requestId = 0;

function deligateDBAccessRequest(operation,data,res,req,requestId){

	console.log("handleDBRequest - " + operation +" with requestId : " + requestId);

	switch(operation){
	case "createUser" :
		console.log("CreateUser for Mongo");
		var bcrypt = require('bcrypt');

		//Getting User Object from Request
		var FIRST_NAME = data.firstname;
		var LAST_NAME = data.lastname;
		var EMAIL_ADDR = data.emailM;
		var DATE_OF_BIRTH = data.dateofbirth;
		var PASSWORD = data.password;
		var GENDER = data.gender;

		if(FIRST_NAME != null && FIRST_NAME != "" 
			&& LAST_NAME != null && LAST_NAME != ""
				&& EMAIL_ADDR != null && EMAIL_ADDR != ""
					&& DATE_OF_BIRTH != null && DATE_OF_BIRTH != ""
						&& PASSWORD != null && PASSWORD != ""
							&& GENDER != null && GENDER != ""){

			//Getting Salt Value and Encrypting Password
			var salt = bcrypt.genSaltSync(10);
			data.password = bcrypt.hashSync(PASSWORD, salt);	

			//Debug information
			console.log(FIRST_NAME);
			console.log(LAST_NAME);
			console.log(EMAIL_ADDR);
			console.log(DATE_OF_BIRTH);
			console.log(PASSWORD);
			console.log(GENDER);
			console.log(PASSWORD);
			console.log(data.IMAGE_URL);

			if(data.IMAGE_URL == null || data.IMAGE_URL == "")
				data.IMAGE_URL = "/images/profile-photo_default.jpg";

			var fieldmap = { firstname: "FIRST_NAME" , lastname: "LAST_NAME" , emailM: "EMAIL_ADDR" , dateofbirth: "DATE_OF_BIRTH" , password: "PASSWORD" , gender: "GENDER",IMAGE_URL : "IMAGE_URL"};

			var newuser = {};

			for (var key in data) {
				if (data.hasOwnProperty(key) && fieldmap[key] != null && fieldmap[key] != "") {
					console.log(key + " -> " + data[key]);
					newuser[fieldmap[key]] = data[key]
				}
			}
			console.log("newuser to be created : " + newuser);
			var sql_stmt = 'INSERT INTO `USERS`(`FIRST_NAME`, `LAST_NAME`, `EMAIL_ADDR`, `DATE_OF_BIRTH`, `PASSWORD`, `GENDER`, `IMAGE_URL`) VALUES (?,?,?,?,?,?,?)';
			executeInsertQuery(sql_stmt,newuser,req,res,operation,requestId);
		}
		else{
			var accountoperation = global.accountoperation;
			accountoperation.userCreationError(res, "There was an Error Creating the user due to missing information!! Please try Again later.", data, req);
		}
		break;

	case "verifyUser" :
	case "loginUser" :
		console.log("loginUser : " + operation);	
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
		}

		if(data.username != null && data.username != ""){
			if( data.username == null ||  data.username == "")
				data.username = req.session.username;
			var sql_query = "SELECT * FROM USERS WHERE EMAIL_ADDR = ?";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);	
		}
		else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Your Session is no Longer Valid!! Please login again to Proceed.",{},req);
		}
		break;

	case "getNewsFeed" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			var sql_query = "SELECT news.POST_MESSAGE as 'POST_MESSAGE',user.FIRST_NAME AS 'FIRST_NAME',user.IMAGE_URL as'IMAGE_URL',user.LAST_NAME AS 'LAST_NAME'";
			sql_query += "FROM NEWSFEEDS news, USERS user where user.ROW_ID = news.USER_ID AND ";
			sql_query += "(news.USER_ID = ? OR news.USER_ID IN (SELECT USER2 FROM FRIENDS_LIST WHERE USER1 = ?" 
			sql_query += " AND ACCEPTED = 'Y')) ORDER BY news.TIMESTAMP DESC";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}
		break;

	case "postStatusUpdate" :
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;

			var newpost = {};

			newpost.USER_ID = req.session.ROW_ID;
			newpost.POST_MESSAGE = req.body.POST_MESSAGE;
			newpost.FIRST_NAME = req.session.firstname;
			newpost.LAST_NAME = req.session.lastname;
			newpost.IMAGE_URL = req.session.IMAGE_URL; 
			
			var sql_stmt = "INSERT INTO `NEWSFEEDS`(`USER_ID`, `POST_MESSAGE`, `IMAGE_URL`)"
				sql_stmt += "VALUES (?,?,?)";

			console.log("postStatusUpdate newpost : " + newpost);
			executeInsertQuery(sql_stmt,newpost,req,res,operation,requestId);

		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadFriendList" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			var sql_query = "SELECT ROW_ID, FIRST_NAME, LAST_NAME, EMAIL_ADDR, DATE_OF_BIRTH,GENDER"
				sql_query +=", IMAGE_URL FROM USERS WHERE ROW_ID <> ?" 
				sql_query += " AND ROW_ID NOT IN (SELECT USER2 FROM FRIENDS_LIST WHERE USER1 = ?) LIMIT 15";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "sendFiendRequest" :
		if(req.session.username != null && req.session.username != ""){
			var newfriendReq = {};
			newfriendReq.username =  req.session.username;
			newfriendReq.firstname = req.session.firstname;
			newfriendReq.lastname = req.session.lastname;
			newfriendReq.ROW_ID = req.session.ROW_ID;
			newfriendReq.IMAGE_URL = req.session.IMAGE_URL;
			newfriendReq.USER1 = req.session.ROW_ID;
			newfriendReq.USER2 = req.body.friend.ROW_ID;
			newfriendReq.ACCEPTED = "N";
			var sql_stmt = 'INSERT INTO `FRIENDS_LIST`(`USER1`, `USER2`, `ACCEPTED`) VALUES (?,?,?)';
			executeInsertQuery(sql_stmt,newfriendReq,req,res,operation,requestId);
		}else{
			var accountoperation =  global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "renderFriendListPage" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			var sql_query = "SELECT ROW_ID, FIRST_NAME, LAST_NAME, EMAIL_ADDR, DATE_OF_BIRTH,GENDER, IMAGE_URL FROM USERS WHERE ROW_ID = ?";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation =  global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadMyFriendList" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			var sql_query = "SELECT ROW_ID, FIRST_NAME, LAST_NAME, EMAIL_ADDR, DATE_OF_BIRTH,GENDER, IMAGE_URL FROM USERS WHERE ROW_ID ";
				sql_query += "IN (SELECT USER2 FROM FRIENDS_LIST WHERE USER1 = ? AND ACCEPTED = 'Y')";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadPendingFriendList" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			var sql_query = "SELECT ROW_ID, FIRST_NAME, LAST_NAME, EMAIL_ADDR, DATE_OF_BIRTH,GENDER, IMAGE_URL FROM USERS WHERE ROW_ID IN (SELECT USER1 FROM FRIENDS_LIST WHERE USER2 = ? AND ACCEPTED = 'N')";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "rejectFriendRequest" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.USER2 = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			data.USER1 = req.body.ROW_ID;
			var sql_query = "DELETE FROM FRIENDS_LIST WHERE USER1 = ? AND USER2 = ? AND ACCEPTED = 'N'";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation =  global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req);
		}
		break;

	case "acceptFriendRequest" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.USER2 = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			data.USER1 = req.body.ROW_ID;
			data.ACCEPTED = "Y";
			var sql_query = "UPDATE FRIENDS_LIST SET ACCEPTED = 'Y' WHERE USER1 = ? AND USER2 = ? AND ACCEPTED = 'N';";
			sql_query += "INSERT INTO FRIENDS_LIST (USER1, USER2, ACCEPTED) VALUES (?,?,?);";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "renderUserDetailsPage" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			var sql_query = "SELECT usd.PHONE as 'PHONE',usd.CURR_CITY as 'CURR_CITY',usd.ABOUT_ME,usd.HOME_ADDR as 'HOME_ADDR', usd.WEB_URL as 'WEB_URL', usd.PROFESSIONAL_SKILLS as 'PROFESSIONAL SKILLS', usd.COMPANY as 'COMPANY', usd.COLLEGE as 'COLLEGE', usd.HIGH_SCHL as 'HIGH_SCHL',usr.ROW_ID as 'ROW_ID', usr.FIRST_NAME as 'FIRST_NAME', usr.LAST_NAME as 'LAST_NAME', usr.EMAIL_ADDR as 'EMAIL_ADDR', usr.DATE_OF_BIRTH as 'DATE_OF_BIRTH', usr.PASSWORD as 'PASSWORD', usr.GENDER as 'GENDER', usr.IMAGE_URL as 'IMAGE_URL' FROM USER_DETAILS usd , USERS usr WHERE usd.USER_ID = usr.ROW_ID AND usr.ROW_ID = ?";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getLifeEvents" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			var sql_query = "SELECT ROW_ID, USER_ID, EVENT_NAME, DATE FROM LIFE_EVENTS WHERE USER_ID = ? ORDER BY DATE DESC";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation =  global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "updateProfilePicture" :
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			req.session.IMAGE_URL = data.newPath;
			var sql_query = "UPDATE USERS SET IMAGE_URL = ? WHERE ROW_ID = ?";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "renderGroupsPage" :
		if(req.session.username != null && req.session.username != ""){
			data ={};
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			var sql_query = "SELECT usd.PHONE as 'PHONE',usd.CURR_CITY as 'CURR_CITY',usd.ABOUT_ME,usd.HOME_ADDR as 'HOME_ADDR', usd.WEB_URL as 'WEB_URL', usd.PROFESSIONAL_SKILLS as 'PROFESSIONAL SKILLS', usd.COMPANY as 'COMPANY', usd.COLLEGE as 'COLLEGE', usd.HIGH_SCHL as 'HIGH_SCHL',usr.ROW_ID as 'ROW_ID', usr.FIRST_NAME as 'FIRST_NAME', usr.LAST_NAME as 'LAST_NAME', usr.EMAIL_ADDR as 'EMAIL_ADDR', usr.DATE_OF_BIRTH as 'DATE_OF_BIRTH', usr.PASSWORD as 'PASSWORD', usr.GENDER as 'GENDER', usr.IMAGE_URL as 'IMAGE_URL' FROM USER_DETAILS usd , USERS usr WHERE usd.USER_ID = usr.ROW_ID AND usr.ROW_ID = ? ";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadAllGroups" :
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			var sql_query ="SELECT * FROM `GROUPS` WHERE ROW_ID NOT IN (SELECT `GROUP_ID`FROM GROUP_USERS where USER_ID = ?)";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "loadMyGroups" :
		if(req.session.username != null && req.session.username != ""){
			data.username =  req.session.username;
			data.firstname = req.session.firstname;
			data.lastname = req.session.lastname;
			data.ROW_ID = req.session.ROW_ID;
			data.IMAGE_URL = req.session.IMAGE_URL;
			var sql_query ="SELECT * FROM GROUPS WHERE ROW_ID IN (SELECT GROUP_ID FROM GROUP_USERS where USER_ID = ?)";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "addUserToGroup" :
		if(req.session.username != null && req.session.username != ""){
			data.USER_ID = req.session.ROW_ID;
			var sql_query ="INSERT INTO GROUP_USERS (GROUP_ID, USER_ID) VALUES (?,?)";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "removeUserFromGroup" :
		if(req.session.username != null && req.session.username != ""){
			data.USER_ID = req.session.ROW_ID;
			var sql_query ="DELETE FROM GROUP_USERS WHERE USER_ID = ? AND GROUP_ID = ?";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "createGroup" :
		if(req.session.username != null && req.session.username != ""){
			data.CREATED_BY = req.session.ROW_ID;
			var sql_query = "INSERT INTO GROUPS(GROUP_NAME, GROUP_INFO, IMAGE_URL,CREATED_BY) VALUES";
				sql_query += "(?,?,?,?);"
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "unFriendUserRequest" :
		if(req.session.username != null && req.session.username != ""){
			data = {};
			data.ROW_ID = req.session.ROW_ID;
			data.friendid = req.body.ROW_ID;
			var sql_query = "DELETE FROM FRIENDS_LIST WHERE USER1 = ? AND USER2 = ? AND ACCEPTED = 'Y';";
				sql_query += "DELETE FROM FRIENDS_LIST WHERE USER2 = ? AND USER1 = ? AND ACCEPTED = 'Y';";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "navToGroupDetailPage" :
		if(req.session.username != null && req.session.username != ""){
			data.ROW_ID = req.session.ROW_ID;
			var sql_query = "SELECT `ROW_ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL_ADDR`, `DATE_OF_BIRTH`, `GENDER`, `IMAGE_URL` FROM `USERS` WHERE ROW_ID= ? ;";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getGroupDetails" :
		if(req.session.username != null && req.session.username != ""){
			data.groupid = parseInt(req.session.groupid);
			var sql_query ="SELECT * FROM `GROUPS` WHERE ROW_ID = ?;";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getGroupUserList" :
		if(req.session.username != null && req.session.username != ""){
			data.groupid = parseInt(req.session.groupid);
			var sql_query = "SELECT `ROW_ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL_ADDR`, `DATE_OF_BIRTH`,`GENDER`, `IMAGE_URL` FROM `USERS` WHERE ROW_ID IN (SELECT USER_ID FROM `GROUP_USERS` WHERE GROUP_ID = ?);";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getGroupNonMembers" :
		if(req.session.username != null && req.session.username != ""){
			data.groupid = parseInt(req.session.groupid);
			var sql_query = "SELECT `ROW_ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL_ADDR`, `DATE_OF_BIRTH`,`GENDER`, `IMAGE_URL` FROM `USERS` WHERE ROW_ID NOT IN (SELECT USER_ID FROM `GROUP_USERS` WHERE GROUP_ID = ?) LIMIT 15;";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;


	case "addUserToGroupAdmin" :
		if(req.session.username != null && req.session.username != ""){
			data.GROUP_ID = parseInt(data.GROUP_ID);
			var sql_query ="INSERT INTO GROUP_USERS (GROUP_ID, USER_ID) VALUES (?,?)";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "removeUserFromGroupAdmin" :
		if(req.session.username != null && req.session.username != ""){
			data.GROUP_ID = parseInt(data.GROUP_ID);
			var sql_query ="DELETE FROM GROUP_USERS WHERE USER_ID = ? AND GROUP_ID = ?;";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "deleteGroup" :
		if(req.session.username != null && req.session.username != ""){
			data = {};
			data.GROUP_ID = parseInt(req.session.groupid);
			var sql_query ="DELETE FROM GROUPS WHERE ROW_ID = ?;";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "navToFriendDetailPage" :
		if(req.session.username != null && req.session.username != ""){
			data = {};
			data.ROW_ID = req.session.ROW_ID;
			var sql_query = "SELECT `ROW_ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL_ADDR`, `DATE_OF_BIRTH`, `GENDER`, `IMAGE_URL` FROM `USERS` WHERE ROW_ID= ?;";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;

	case "getFriendDetails" :
		if(req.session.username != null && req.session.username != ""){
			data = {};
			data.friendid = parseInt(req.session.friendid);
			var sql_query = "SELECT usd.PHONE as 'PHONE',usd.CURR_CITY as 'CURR_CITY',usd.ABOUT_ME,usd.HOME_ADDR as 'HOME_ADDR', usd.WEB_URL as 'WEB_URL', usd.PROFESSIONAL_SKILLS as 'PROFESSIONAL SKILLS', usd.COMPANY as 'COMPANY', usd.COLLEGE as 'COLLEGE', usd.HIGH_SCHL as 'HIGH_SCHL',usr.ROW_ID as 'ROW_ID', usr.FIRST_NAME as 'FIRST_NAME', usr.LAST_NAME as 'LAST_NAME', usr.EMAIL_ADDR as 'EMAIL_ADDR', usr.DATE_OF_BIRTH as 'DATE_OF_BIRTH', usr.PASSWORD as 'PASSWORD', usr.GENDER as 'GENDER', usr.IMAGE_URL as 'IMAGE_URL' FROM USER_DETAILS usd , USERS usr WHERE usd.USER_ID = usr.ROW_ID AND usr.ROW_ID =?";
			executeSelectQuery(sql_query,data,req,res,operation,requestId);
		}else{
			var accountoperation = global.accountoperation;
			accountoperation.userUnverified(res,"Invalid Session!! Please Login to continue.",{},req)
		}
		break;


	default:
		break;
	}
}

function executeSelectQuery(sql_stmt,data,req,res,operation,requestId){
	try {
		console.log("executeSelectQuery for " + operation + " : Sending Data : " + JSON.stringify(data));
		soap.makeRequest(operation,sql_stmt, data, function(error, response) {
			console.log("Response from Mongo for " + operation + "  : "  + JSON.stringify(response));
			if (!response || response.code == 200) {
				if (operation == "verifyUser"
					|| operation == "loginUser" 
						|| operation == "renderFriendListPage"
							|| operation == "renderUserDetailsPage" 
								|| operation == "updateProfilePicture" 
									|| operation == "renderGroupsPage"
										|| operation == "navToGroupDetailPage"
											|| operation == "deleteGroup"
												|| operation == "navToFriendDetailPage") {
					try {
						console.log("exisiting data : " + JSON.stringify(data));
						if (data == null || data == "")
							data = {};
						if (req.session.username != ""	&& req.session.username != null) {
							console.log("Session found User Detials being returned !!");
							var fieldmap = {
									ROW_ID : "ROW_ID",
									FIRST_NAME : "firstname",
									LAST_NAME : "lastname",
									EMAIL_ADDR : "emailM",
									DATE_OF_BIRTH : "dateofbirth",
									GENDER : "gender",
									IMAGE_URL : "IMAGE_URL"
							};

							for ( var key in response) {
								if (response.hasOwnProperty(key) && fieldmap[key] != null && fieldmap[key] != "" && response[key] != ""	&& response[key] != null) {
									data[fieldmap[key]] = response[key];
								}
							}
							console.log("data  Final before operations: " + data);
							if (operation == "verifyUser")
								accountoperation.userVerified(data, res, req);
							else if (operation == "loginUser") {
								console.log("Test" + operation);
								accountoperation.homeRedirection(data, res, req);
							}else if(operation == "renderFriendListPage"){
								console.log("Test" + operation);
								accountoperation.FriendListPageRedirect(data,res, req);
							}else if(operation == "renderUserDetailsPage"){
								console.log("Test" + operation);
								accountoperation.userDetailsPageRedirect(response,res, req);
							}else if(operation == "updateProfilePicture"){
								accountoperation.renderUserDetailsPage({},	res, req);
							}else if(operation == "renderGroupsPage"){
								console.log("Test" + operation);
								accountoperation.groupPageRedirect(response,res, req);
							}else if(operation == "navToGroupDetailPage"){
								console.log("Test" + operation);
								accountoperation.groupDetailsPageRedirect(response,res, req);
							}else if (operation == "deleteGroup") {
								console.log("Test" + operation);
								accountoperation.userVerified(data,	res, req);
							}else if(operation == "navToFriendDetailPage"){
								console.log("Test" + operation);
								accountoperation.friendDetailsPageRedirect(response,res, req);	
							}
						} else {
							console.log("no session found!!");
							var bcrypt = require('bcrypt');
							if (bcrypt.compareSync(data.password,response.PASSWORD)) {

								var fieldmap = { ROW_ID : "ROW_ID",FIRST_NAME : "firstname",LAST_NAME : "lastname",EMAIL_ADDR : "emailM",DATE_OF_BIRTH : "dateofbirth",GENDER : "gender",IMAGE_URL : "IMAGE_URL"};

								for ( var key in response) {
									if (response.hasOwnProperty(key) && fieldmap[key] != null	&& fieldmap[key] != ""	&& response[key] != "" && response[key] != null) {
										console.log(key	+ " No Session -> "	+ response[key]);
										data[fieldmap[key]] = response[key];
									}
								}
								accountoperation.userVerified(data, res, req);
								console.log("User Verified !!! ");
							} else {
								console.log("User Not Verfied !!! ");
								data.password = "";
								accountoperation.userUnverified(res,"Incorrect username or Password Specified",	data, req);
							}
						}

					} catch (e) {
						console.log(e.toString());
						console.log("User Not Verfied !!! ");
						data.password = "";
						accountoperation.userUnverified(res,"Incorrect username or Password Specified",	data, req);
					}
				}
				//Async Request which dont needs rendering of pages
				if (operation == "getNewsFeed"
					|| operation == "loadFriendList"
						|| operation == "loadMyFriendList"
							|| operation == "loadPendingFriendList"
								|| operation == "rejectFriendRequest"
									|| operation == "acceptFriendRequest" 
										|| operation == "getLifeEvents"
											|| operation == "loadAllGroups"
												|| operation == "loadMyGroups"
													||operation == "addUserToGroup"
														|| operation == "removeUserFromGroup"
															|| operation == "createGroup"
																|| operation == "unFriendUserRequest"
																	|| operation == "getGroupDetails"
																		|| operation == "getGroupUserList"
																			|| operation == "getGroupNonMembers"
																				|| operation == "addUserToGroupAdmin"
																					|| operation == "removeUserFromGroupAdmin"
																						|| operation == "getFriendDetails") {
					try{
						if(response)
							res.status(200).send(JSON.stringify(response.QueryResult));
						else
							res.status(401).send();
					}catch(e){
						console.log(e);
						res.status(401).send();
					}

				}
			}else{
				if (operation == "verifyUser" 
					|| operation == "loginUser"
						||  operation == "renderFriendListPage"
							|| operation == "renderUserDetailsPage"
								|| operation == "updateProfilePicture"
									|| operation == "renderGroupsPage"
										|| operation == "navToGroupDetailPage"
											|| operation == "deleteGroup"
												|| operation == "navToFriendDetailPage") {
					var newuser = {};
					newuser.username = data.username;
					console.log(error);
					console.log(newuser);
					accountoperation.userUnverified(res,"Incorrect username or Password Specified",newuser, req);
				}
				if (operation == "getNewsFeed"
					|| operation == "loadFriendList"  
						|| operation == "loadMyFriendList"
							|| operation == "loadPendingFriendList" 
								|| operation == "rejectFriendRequest"
									|| operation ==  "acceptFriendRequest"
										|| operation == "getLifeEvents"
											||operation == "loadAllGroups"
												|| operation == "loadMyGroups"
													||operation == "addUserToGroup"
														|| operation == "removeUserFromGroup"
															|| operation == "createGroup"
																|| operation == "unFriendUserRequest"
																	|| operation == "getGroupDetails"
																		|| operation == "getGroupUserList"
																			|| operation == "getGroupNonMembers"
																				|| operation == "addUserToGroupAdmin"
																					|| operation == "removeUserFromGroupAdmin"
																						|| operation == "getFriendDetails") {
					res.status(403).send(error);
				}

			}
		});
	} catch (e) {
		console.log(e);
	}finally{}
}

function executeInsertQuery(sql_stmt,data,req,res,operation,requestId){
	try {
		console.log("Got Connection and Executing Query !! (requestId) ==> "+ requestId +" ==>> " + sql_stmt);
		soap.makeRequest(operation,sql_stmt, data, function(err, response) {
			console.log("Response from Mongo " + JSON.stringify(response));
			if (!response || response.code == 200) {
				if (operation === "createUser") {
					var fieldmap = {
							FIRST_NAME : "firstname",
							LAST_NAME : "lastname",
							EMAIL_ADDR : "emailM",
							DATE_OF_BIRTH : "dateofbirth",
							PASSWORD : "password",
							GENDER : "gender",
							IMAGE_URL : "IMAGE_URL"
					};

					var newuser = {};

					for ( var key in data) {
						if (data.hasOwnProperty(key) && fieldmap[key] != null
								&& fieldmap[key] != "" && data[key] != ""
									&& data[key] != null) {
							console.log(key + " -> " + data[key]);
							newuser[fieldmap[key]] = data[key]
						}
					}
					newuser.ROW_ID = response.ROW_ID;
					console.log("Final newuser : " + JSON.stringify(newuser));
					accountoperation.userCreated(res, newuser, req);
				}
				if (operation == "postStatusUpdate" || operation == "sendFiendRequest") {
					res.status(200).send(response);
				}
			} else {
				if (operation === "createUser") {
					var fieldmap = {
							FIRST_NAME : "firstname",
							LAST_NAME : "lastname",
							EMAIL_ADDR : "emailM",
							DATE_OF_BIRTH : "dateofbirth",
							PASSWORD : "password",
							GENDER : "gender"
					};

					var newuser = {};

					for ( var key in data) {
						if (data.hasOwnProperty(key) && fieldmap[key] != null
								&& fieldmap[key] != "" && data[key] != ""
									&& data[key] != null) {
							console.log(key + " -> " + data[key]);
							newuser[fieldmap[key]] = data[key]
						}
					}
					console.log(response.err);
					console.log(newuser);
					accountoperation.userCreationError(res, response.err, newuser, req);
				}
				if (operation == "postStatusUpdate" || operation == "sendFiendRequest") {
					res.status(403).send(err);
				}
			}
		});
	} catch (e) {
		console.log(e);
	}
	finally{	
	}
}

module.exports = {
	handleDBRequest : function(operation,data,res,req){
		console.log("handleDBRequest got the Request to '" + operation +"' and its handled with RequestId : " + requestId);
		deligateDBAccessRequest(operation,data,res,req,requestId);
		requestId++;
	}
};
